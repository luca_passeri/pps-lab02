package u02lab.code;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

public class RangeGeneratorTest {

    private static final int START_VALUE = 2;
    private static final int STOP_VALUE = 4;

    private SequenceGenerator rangeGenerator;

    @Before
    public void init() {
        this.rangeGenerator = SequenceGeneratorFactory.createRangeGenerator(START_VALUE, STOP_VALUE);
    }

    @Test
    public void next() {
        Optional<Integer> nextValue = this.rangeGenerator.next();
        Assert.assertTrue(nextValue.isPresent());
        Assert.assertEquals(new Integer(2), nextValue.get());

        this.rangeGenerator.next();
        nextValue = this.rangeGenerator.next();
        Assert.assertFalse(nextValue.isPresent());
    }

    @Test
    public void reset() {
        this.rangeGenerator.next();
        this.rangeGenerator.reset();
        Optional<Integer> nextValue = this.rangeGenerator.next();

        Assert.assertTrue(nextValue.isPresent());
        Assert.assertEquals(new Integer(2), nextValue.get());
    }

    @Test
    public void isOver() {
        this.rangeGenerator.next();
        Assert.assertFalse(this.rangeGenerator.isOver());

        this.rangeGenerator.next();
        Assert.assertTrue(this.rangeGenerator.isOver());
    }

    @Test
    public void allRemaining() {
        int current = START_VALUE;
        List<Integer> allRemaining = this.rangeGenerator.allRemaining();

        for (int i : allRemaining) {
            Assert.assertEquals(i, current);
            current++;
        }

        current = START_VALUE;
        current++;
        this.rangeGenerator.next();

        allRemaining = this.rangeGenerator.allRemaining();

        for (int i : allRemaining) {
            Assert.assertEquals(i, current);
            current++;
        }
    }
}