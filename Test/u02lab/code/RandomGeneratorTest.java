package u02lab.code;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.swing.text.html.Option;

import java.util.Optional;

import static org.junit.Assert.*;

public class RandomGeneratorTest {

    private static final int SEQUENCE_SIZE = 3;

    private SequenceGenerator randomGenerator;

    @Before
    public void init() {
        this.randomGenerator = SequenceGeneratorFactory.createRandomGenerator(SEQUENCE_SIZE);
    }

    @Test
    public void next() {
        Optional<Integer> nextValue = this.randomGenerator.next();
        Assert.assertTrue(nextValue.isPresent());
        Assert.assertTrue(nextValue.get()==0 || nextValue.get()==1);

        nextValue = this.randomGenerator.next();
        Assert.assertTrue(nextValue.isPresent());
        Assert.assertTrue(nextValue.get()==0 || nextValue.get()==1);

        nextValue = this.randomGenerator.next();
        Assert.assertTrue(nextValue.isPresent());
        Assert.assertTrue(nextValue.get()==0 || nextValue.get()==1);

        nextValue = this.randomGenerator.next();
        Assert.assertFalse(nextValue.isPresent());
    }

    @Test
    public void reset() {
        this.randomGenerator.next();
        this.randomGenerator.next();
        this.randomGenerator.next();
        this.randomGenerator.reset();

        Optional<Integer> nextValue = this.randomGenerator.next();
        Assert.assertTrue(nextValue.isPresent());
        Assert.assertTrue(nextValue.get()==0 || nextValue.get()==1);
    }

    @Test
    public void isOver() {
        this.randomGenerator.next();
        Assert.assertFalse(this.randomGenerator.isOver());
        this.randomGenerator.next();
        Assert.assertFalse(this.randomGenerator.isOver());
        this.randomGenerator.next();
        Assert.assertTrue(this.randomGenerator.isOver());
        this.randomGenerator.next();
        Assert.assertTrue(this.randomGenerator.isOver());
    }

    @Test
    public void allRemaining() {
        Assert.assertEquals(this.randomGenerator.allRemaining().size(), 3);
        this.randomGenerator.next();
        Assert.assertEquals(this.randomGenerator.allRemaining().size(), 2);
        this.randomGenerator.next();
        Assert.assertEquals(this.randomGenerator.allRemaining().size(), 1);
        this.randomGenerator.next();
        Assert.assertEquals(this.randomGenerator.allRemaining().size(), 0);
    }
}