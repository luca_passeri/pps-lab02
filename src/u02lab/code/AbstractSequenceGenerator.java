package u02lab.code;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class AbstractSequenceGenerator implements SequenceGenerator {

    protected final int start;
    protected final int stop;
    protected int current;

    public AbstractSequenceGenerator(int sequenceLength) {
        this.start=0;
        this.stop = sequenceLength;
        this.current = 0;
    }

    public AbstractSequenceGenerator(int start, int stop){
        this.start = start;
        this.stop = stop;
        this.current = start;
    }

    @Override
    public Optional<Integer> next() {
        return current<stop?Optional.of(nextValue()):Optional.empty();
    }

    @Override
    public void reset() {
        this.current = this.start;
    }

    @Override
    public boolean isOver() {
        return this.current == this.stop;
    }

    @Override
    public List<Integer> allRemaining() {
        return Collections.unmodifiableList(getRange()
                .collect(Collectors.toList()));
    }

    protected abstract Stream<Integer> getRange();

    protected abstract int nextValue();


}
