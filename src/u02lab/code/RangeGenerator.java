package u02lab.code;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Step 1
 *
 * Using TDD approach (create small test, create code that pass test, refactor to excellence)
 * implement the below class that represents the sequence of numbers from start to stop included.
 * Be sure to test that:
 * - the produced elements (called using next(), go from start to stop included)
 * - calling next after stop leads to a Optional.empty
 * - calling reset after producing some elements brings the object back at the beginning
 * - isOver can actually be called in the middle and should give false, at the end it gives true
 * - can produce the list of remaining elements in one shot
 */
public class RangeGenerator extends AbstractSequenceGenerator {


    public RangeGenerator(int start, int stop){
        super(start, stop);
    }

    @Override
    protected Stream<Integer> getRange() {
        return IntStream.rangeClosed(current, stop)
                .boxed();
    }

    @Override
    protected int nextValue() {
        return this.current++;
    }
}
