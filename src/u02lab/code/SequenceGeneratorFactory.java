package u02lab.code;

public final class SequenceGeneratorFactory {

    private SequenceGeneratorFactory() {}

    public static AbstractSequenceGenerator createRangeGenerator(int start, int stop) {
        return new RangeGenerator(start, stop);
    }

    public static AbstractSequenceGenerator createRandomGenerator(int sequenceLength) {
        return new RandomGenerator(sequenceLength);
    }
}
