package u02lab.code;

import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Step 2
 *
 * Now consider a RandomGenerator. Using TDD approach (create small test, create code that pass test, refactor
 * to excellence) implement the below class that represents a sequence of sequenceSize random bits (0 or 1). Recall
 * that math.random() gives a double in [0,1]..
 * Be sure to test all that is needed, as before
 *
 * When you are done:
 * A) try to refactor the code according to DRY (RandomGenerator vs RangeGenerator), using patterns as needed
 * - be sure tests still pass
 * - refactor the test code as well
 * B) create an abstract factory for these two classes, and implement it
 */
public class RandomGenerator extends AbstractSequenceGenerator {

    private static final Random random = new Random();

    public RandomGenerator(int sequenceLength) {
        super(sequenceLength);
    }

    @Override
    protected Stream<Integer> getRange() {
        return IntStream.range(this.current, this.stop)
                .mapToObj(x -> randomValue());
    }

    @Override
    protected int nextValue() {
        this.current++;
        return randomValue();
    }

    private int randomValue() {
        return random.nextInt(2);
    }
}
